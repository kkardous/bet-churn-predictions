for name in dir():
    if not name.startswith('_'):
        del globals()[name]
        
# credentials = credentials.
import google
import os
from google.cloud import bigquery
import pandas as pd
from functools import reduce
import numpy as np
from sklearn.model_selection import train_test_split
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.model_selection import GroupShuffleSplit
import json
import logging
import argparse
import configparser
from collections import OrderedDict
from datetime import datetime
from pathlib import Path
from pympler import asizeof
import re
import mparticle
# import mparticle
from mparticle import AppEvent, Batch
from datetime import datetime, date, time, timedelta
from pytz import timezone    
from matplotlib.dates import date2num
import math

# env variable
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] =  'oath_creds_bq4.json'

# variables 
GCP_PROJECT = 'nv2ift1'
BQ_DATASET = 'mParticleTest'

# connections
client = bigquery.Client(project = GCP_PROJECT)
dataset_ref = client.dataset(BQ_DATASET)

# results to datafrme function 
def gcp2df(sql):
    query = client.query(sql)
    results = query.result()
    return results.to_dataframe()

# gcp2df(query)

# setting up configurations and functions to prepare to write to mparticle
# conf file 
# os.chdir("C:\\Users\\kkardous\OneDrive - Bet.Works\Documents\sql_queries\churn_lapsing_signs")
cwd = os.path.dirname(os.path.abspath('/home/kkardous/write_to_mparticle'))
CONFIG_FILE_NAME = 'rd.properties.conf'
CONFIG_FILE_PATH =  os.path.join(cwd, CONFIG_FILE_NAME)
SECTION_DUMMY = 'dummy_section'
SECTION_FILE = 'file'
SECTION_SIZE = 'size'
SECTION_KEY = 'key'
SECTION_ENV = 'env'
REPORT_PATH_FIELD = 'report_path'
TRANSACTION_FILES_FIELD = 'transaction_files'
USER_PREFERENCE_FILES_FIELD = 'user_preference_files'
MAX_BATCH_SIZE_FIELD = 'max_batch_size'
MAX_BATCH_COUNT_FIELD = 'max_batch_count'
MAX_EVENT_COUNT_FIELD = 'max_event_count'
RECENT_TIME_FIELD = 'recent_time_in_seconds'
TIMEOUT_FIELD = 'timeout'
TOTAL_RETRY_FIELD = 'TOTAL_RETRY'
CURRENT_ENV_FIELD = 'current_env'
BRAND_ID_LABEL = 'BRAND_ID'
BETS_WITH_DETAILS_LABEL = 'BetsWithDetails'
BETS_LABEL = 'Bets'
CUSTOMER_PROFILES_LABEL = 'CustomerProfiles'
CUSTOMER_BALANCE_FILES_LABEL = 'CustomerBalances'
BONUS_TRANSACTION_FILES_LABEL = 'BonusTransactions'
BETLEGS_LABEL = 'Bet Legs'
CRS_LABEL = 'CRS'
EMAILOPTIN_LABEL = 'Email Opt In'
USERNAME_LABEL = 'Username'
EMAIL_LABEL = 'Email'
MOBILE_PHONE_LABEL = 'Mobile Phone Number'
FIRST_NAME_LABEL = 'First Name'
LAST_NAME_LABEL = 'Last Name'
ACCOUNT_BALANCE_LABEL = 'Account Balance'
ACCOUNT_TYPE_LABEL = 'Account Type'
CASH_BALANCE_LABEL = 'Cash Balance'
GENERIC_BONUS_BALANCE_LABEL = 'Generic Bonus Balance'
SBK_BONUS_BALANCE_LABEL = 'SBK Bonus Balance'
CAS_BONUS_BALANCE_LABEL = 'CAS Bonus Balance'
CASH_ACCOUNT_TYPE = 'CASH'
BONUS_GENERIC_ACCOUNT_TYPE = 'BONUS_GENERIC'
BONUS_SBK_ACCOUNT_TYPE = 'BONUS_SBK'
BONUS_CAS_ACCOUNT_TYPE = 'BONUS_CAS'
CUSTOM_EVENT_TYPE_OTHER = 'other'
CUSTOM_EVENT_TYPE_TRANSACTION = 'transaction'
CUSTOM_EVENT_TYPE_USER_PREFERENCE = 'user_preference'
EVENT_NAME_BETS = 'bets'
EVENT_NAME_PLACED_BETS = 'placed_bets'
EVENT_NAME_CUSTOMER_OPT_IN_PROGRAMS = 'customer_opt_in_programs'
EVENT_NAME_CUSTOMER_BONUS_OPT_INS = 'customer_bonus_opt_ins'
EVENT_NAME_FREE_BET_COUPONS = 'free_bet_coupons'
EVENT_NAME_AWARDED_FREE_BETS = 'awarded_free_bets'
LAST_MODIFIED_DATE = 'Last Modified Date'
CUSTOMER_ID = 'Customer ID'
BRAND_ID = 'Brand ID'
pattern = re.compile(r'(?<!^)(?=[A-Z])')
sub_pattern = '\(|\)|\"|\ '
label_mapping_dict = {"Registration Platform": "Signup Platform",
                      "Last Modification Date": "Last Modified Date",
                      "Last Modification IP": "Last Modified IP Address",
                      "Signup Terms Conditions": "Terms Version",
                      "Third Party Customer ID": "External Customer ID",
                      "First Mobile Bet Placed": "First Mobile Bet ID",
                      "First Deposit Transaction ID": "First Deposit ID",
                      "Num Legs": "Number Of Legs",
                      "If Win Amount": "Potential Win Amount",
                      "Freebet Coupon ID": "Free Bet ID",
                      "Amount": "Transaction Amount",
                      "Balance": "Account Balance"
                      }
user_data = ['Status', 'Test Account', 'Signup Date', 'Signup Platform', 'Casino ID',
             'Last Login Date', 'Terms Version', 'Terms Accepted Date', 'External Customer ID', 'Affiliate Type',
             'Affiliate ID',  'First Mobile Bet ID', 'First Deposit ID', 'Player Tier', 'Bonus Exempt',
             'Last Modified Date', 'Email Subscribe', 'First Name', 'Last Name', 'Account ID',
             'Account Type', 'Account Status', 'CRS Value']

key_secret_list = []
brand_api_key_dict = {}
with open(CONFIG_FILE_PATH, 'r') as f:
    # config_string = '[dummy_section]\n' + f.read()
    config_string = '[' + SECTION_DUMMY + ']' + '\n' + f.read()
    config = configparser.ConfigParser()
    config.read_string(config_string)
    brand_id_str = config[SECTION_DUMMY][BRAND_ID_LABEL]
    new_brand_id_str = re.sub(sub_pattern, '', brand_id_str)
    brand_id_list = new_brand_id_str.split(',')
    # Get total retry times from config file
    total_retry = int(config[SECTION_DUMMY][TOTAL_RETRY_FIELD])
    max_batch_size = config[SECTION_SIZE][MAX_BATCH_SIZE_FIELD]
    max_batch_count = config[SECTION_SIZE][MAX_BATCH_COUNT_FIELD]
    max_event_count = config[SECTION_SIZE][MAX_EVENT_COUNT_FIELD]
    recent_time = config[SECTION_SIZE][RECENT_TIME_FIELD]
    timeout_duration = config[SECTION_SIZE][TIMEOUT_FIELD]
    current_env = config[SECTION_ENV][CURRENT_ENV_FIELD]
    # Use OrderedDict to preserve the order of key
    key_dict = OrderedDict(config.items(SECTION_KEY))
    for key in key_dict:
        if key.startswith('api_key'):
            key_secret_tuple = ()
            key_secret_tuple += (key_dict[key],)
        elif key.startswith('api_secret'):
            key_secret_tuple += (key_dict[key],)
            key_secret_list.append(key_secret_tuple)
    for idx in range(len(brand_id_list)):
        brand_api_key_dict[brand_id_list[idx]] = key_secret_list[idx]
         
def get_api_instance_by_brand(brand_id):
    if brand_id in brand_api_key_dict:
        (key, secret) = brand_api_key_dict[brand_id]
    else:
        # "None"
        raise Exception('brand_id ' + str(brand_id) + ' not found in brand_api_key_dict.')
    configuration = mparticle.Configuration()
    configuration.api_key = key
    configuration.api_secret = secret
    configuration.debug = False
    api_instance = mparticle.EventsApi(configuration)
    return key, secret # api_instance, key, secret

# converting/mapping brand ids to casino ids

def brand_to_casinoid(brand_id):
    switcher = {
    "2000" : "211",
    "2001" : "211",
    "2002" : "211",
    "2020" : "212",
    "2021" : "212", 
    "2040" : "213",
    "2041" : "213",
    "1020" : "215",
    "2080" : "222",
    "2081" : "222"
    }
    return switcher.get(brand_id, "unknown")

# brand_to_casinoid("211")
# applying function above to generate brand id column
def unnest_key_secret(df):
    df['signup_casino_id'] = [str(x).split(sep = '.')[0] for x in df['signup_casino_id']]
    df['brand_id'] = df['signup_casino_id'].astype(str).apply(lambda x: brand_to_casinoid(x))
    df['api_pairs'] = df['brand_id'].apply(lambda x: get_api_instance_by_brand(x))
    df['key'], df['secret'] = list(zip(*((x[0], x[1]) for x in df['api_pairs'].values)))
    del df['api_pairs']
    return df

# reading data from bigquery client

## bet frequencies
bet_frequency_query = """SELECT * FROM `nv2ift1.mParticleTest.bet_triggers_refresh_table`"""
bet_frequency  = gcp2df(bet_frequency_query)


## pulling additional features such as last balances, free cash balance, last churn factors, etc.
combined_bet_transaction_event_qry = """SELECT 
    customerid,
    MAX(churn_factors_agg) AS customer_lapsing_factor,
    MAX(customer_first_lapsing_factor) AS customer_first_lapsing_factor,
    MAX(customer_last_free_bet_perc) AS customer_last_free_bet_perc,
    MAX(customer_first_free_bet_perc) AS customer_first_free_bet_perc, 
    MAX(customer_last_free_bet_cash_balance) AS customer_last_free_bet_cash_balance,
    MAX(customer_first_free_bet_balance) AS customer_first_free_bet_balance,
    MAX(customer_last_cash_balance) AS customer_last_cash_balance,
    MAX(customer_last_balance_ttl) AS customer_last_balance_ttl,
    MAX(wow_free_balance_change) AS wow_free_balance_change,
    MAX(wow_free_bet_cnt_change) AS wow_free_bet_cnt_change,
    MAX(wow_cash_balance_change) AS wow_cash_balance_change,
    MAX(wow_frequency_change) AS wow_frequency_change,
    MAX(last3wks_ma_frequency) AS last3wks_ma_frequency,
    MAX(last2wks_ma_frequency) AS last2wks_ma_frequency,
    MAX(longevity_ttl) AS longevity,
    MAX(recency_ttl) AS recency,
    MAX(frequency_ttl) AS frequency
    FROM `nv2ift1.mParticleTest.combined_event_transaction_placed_bets`
    GROUP BY 1""" 
combined_bet_transaction_events  = gcp2df(combined_bet_transaction_event_qry)


## pulling bet time preferences 
bet_time_preference_query = """SELECT * FROM `nv2ift1.mParticleTest.combined_bet_times_preferences`"""
bet_time_preference = gcp2df(bet_time_preference_query)

## league preferences
league_preference_query = """SELECT * FROM `nv2ift1.mParticleTest.league_preferences`"""
league_preference = gcp2df(league_preference_query)

## customer dim to get signup casino id 
signup_casino_id_query = """SELECT customer_id AS customerid, signup_casino_id FROM `nv2ift1.mParticleTest.customer_dim`"""
signup_casino_id_ref = gcp2df(signup_casino_id_query)

## adding last sport/league visited per csutomerid
last_visited_sport_query = """SELECT customerid, last_visited_league, CAST(session_start_time AS STRING)  AS sport_visited_start_time  FROM `nv2ift1.mParticleTest.combined_last_sport_page_visited`"""
last_visited_sport_ref = gcp2df(last_visited_sport_query)

# joining on prefered time & league preferences
bet_frequency = reduce(lambda  left, right: 
                              pd.merge(left, right, on = ['customerid'], how = 'left'),
                              [bet_frequency, combined_bet_transaction_events,\
                               signup_casino_id_ref, bet_time_preference, league_preference, last_visited_sport_ref])

# this is to fix nan in customer churn factors and apply coalesce replacing nan with recency divided by bet_freq_filled 
bet_frequency['customer_lapsing_factor'] = bet_frequency['customer_lapsing_factor'].fillna(bet_frequency['recency'] / bet_frequency['bet_freq_filled'])
# also same for bet_frequency
bet_frequency['frequency'] = bet_frequency['frequency'].fillna(bet_frequency['bet_freq_filled'])
# also same for last visited sport page and last visted sport sesssion start time
bet_frequency['last_visited_league'] = bet_frequency['last_visited_league'].fillna('')
bet_frequency['sport_visited_start_time'] = bet_frequency['sport_visited_start_time'].fillna('')

# converting sport vistied sport time to datetime64[ns]
# bet_frequency['sport_visited_start_time']  = bet_frequency['sport_visited_start_time'].astype('datetime64')

# bet_frequency['sport_visited_start_time'] = bet_frequency.sport_visited_start_time.astype(object).where(bet_frequency.sport_visited_start_time.notnull(), 0)
# bet_frequency['sport_visited_start_time']  = bet_frequency['sport_visited_start_time'].astype('datetime64')

# now getting list of customers with 1) triggers and 2) lapsed triggers for today to send message notification
bet_frequency['today'] = datetime.today().date()
trigger_today_customers = bet_frequency[(bet_frequency['event_type'] != 'Bet') & (bet_frequency['event_dates_adj'] == bet_frequency['today'])]
trigger_today_customers = unnest_key_secret(trigger_today_customers)

# now reducing dataframe to include customers to message now
# first classifying current timestamp per customer and due dates based on casino id and local timezones
def fetch_relevant_timezone(df, casino_id):
    casino_ids = df['signup_casino_id']
    timezone = []
    for casino_id in casino_ids:
        if casino_id in ('2020', '6000', '6003', '7030', '2040', '2041', '1030', '1031', '7500'):
            timezone.append('US/Central')
        elif casino_id in ('2000', '6020', '6021', '6000', '6002', '7010'):
            timezone.append('US/Mountain')
        elif casino_id in ('2080', '2081', '7020', '7000'):
            timezone.append('US/Eastern')
        else:
            timezone.append('US/Pacific')
    return timezone
 
trigger_today_customers['tz'] = fetch_relevant_timezone(trigger_today_customers, trigger_today_customers['signup_casino_id'])

def hour_rounder(t):
    return (t.replace(second = 0, microsecond = 0, minute = 0, hour = t.hour)) #+ timedelta(hours = t.minute // 30))

trigger_today_customers['tz_now'] = trigger_today_customers['tz'].apply(lambda x: datetime.now(timezone(x)).strftime('%Y-%m-%d %H:%M:%S')).astype('datetime64')
# applying the rounding to the hour before comparing due dates with current time
trigger_today_customers['tz_now_rounded'] = trigger_today_customers['tz_now'].apply(lambda x: hour_rounder(x))

# now filtering to 'send notification now' customers
mask_bet_due = ( (trigger_today_customers['tz_now_rounded'].dt.hour == trigger_today_customers['prefered_bet_time_of_day'] + 1) & (trigger_today_customers['event_type'] == 'Trigger') )
reduced_df_send_message_now_lapse_prediction = (trigger_today_customers.loc[mask_bet_due])


mask_bet_due_lapsed_customer = ( (trigger_today_customers['tz_now_rounded'].dt.hour == trigger_today_customers['prefered_bet_time_of_day'] + 1) &\
                                (trigger_today_customers['event_type'] == 'Lapsed Trigger') & (trigger_today_customers['recency'] > 2) )
reduced_df_send_message_now_lapsed_customer = (trigger_today_customers.loc[mask_bet_due_lapsed_customer])


# grouping up customers by brand if any for customer lapse prediction 
df_dict = {}
for brand, values in reduced_df_send_message_now_lapse_prediction.groupby('signup_casino_id'):
    df_dict.update({str(brand) : values.reset_index(drop=True)})


# grouping up customers by brand if any for customer lapse prediction 
df_lapsed_dict = {}
for brand, values in reduced_df_send_message_now_lapsed_customer.groupby('signup_casino_id'):
    df_lapsed_dict.update({str(brand) : values.reset_index(drop=True)})

# function to transforme each column needed to a list so that we can include them as lists to dicts in batch further below
def col_to_list(df):
    
    if df.shape[0] > 0:
    # customers = df[['customerid', 'brand_id']].values.tolist()
        customers = df['customerid'].values.tolist()
        brand_ids = df['brand_id'].values.tolist()
        casino_ids = df['signup_casino_id'].values.tolist()
        balances = df['customer_last_balance_ttl'].values.tolist()
        free_bet_cash_balance = df['customer_last_free_bet_cash_balance'].values.tolist()
        wow_free_balance_change = df['wow_free_balance_change'].values.tolist()
        wow_free_bet_cnt_change = df['wow_free_bet_cnt_change'].values.tolist()
        wow_cash_balance_change = df['wow_cash_balance_change'].values.tolist()
        wow_frequency_change = df['wow_frequency_change'].values.tolist()
        last3wks_ma_frequency = df['last3wks_ma_frequency'].values.tolist()
        last2wks_ma_frequency = df['last2wks_ma_frequency'].values.tolist()
        longevity = df['longevity'].values.tolist()
        recency = df['recency'].values.tolist()
        frequency = df['frequency'].values.tolist()
        churn_factor = df['customer_lapsing_factor'].values.tolist()
        league_preference = df['league_preference'].values.tolist()
        last_visited_league = df['last_visited_league'].values.tolist()
        sport_visited_start_time = df['sport_visited_start_time'].values.tolist()
        event_type = df['event_type'].values.tolist()
        api_key = df['key'].values.tolist()
        api_secret = df['secret'].values.tolist()
        

        return customers, brand_ids, casino_ids, balances, free_bet_cash_balance, wow_free_balance_change,\
            wow_free_bet_cnt_change, wow_cash_balance_change, wow_frequency_change, last3wks_ma_frequency,\
                last2wks_ma_frequency, longevity, recency, frequency, churn_factor , league_preference,\
                    last_visited_league, sport_visited_start_time, event_type, api_key, api_secret
    else:
        None

def return_needed_lists_by_brand(brand_id):
    
    list_names = ['customer_id', 'brand_id', 'casino_id', 'balances',\
                  'free_bet_cash_balance', 'wow_free_balance_change',\
                      'wow_free_bet_cnt_change', 'wow_cash_balance_change',\
                          'wow_frequency_change', 'last3wks_ma_frequency',\
                              'last2wks_ma_frequency', 'longevity', 'recency',\
                                  'frequency', 'churn_factor', 'league_preference',\
                                      'last_visited_league', 'sport_visited_start_time', 'event_type'
                                  
                  ]
        
    if len(df_dict.keys()) == 0:
        return  
    if brand_id not in df_dict.keys():
        return  
    else:
        customers = col_to_list(df_dict[brand_id])[0]
        brand_ids = col_to_list(df_dict[brand_id])[1]
        casino_ids = col_to_list(df_dict[brand_id])[2]
        balances = col_to_list(df_dict[brand_id])[3]
        free_bet_cash_balance = col_to_list(df_dict[brand_id])[4]
        wow_free_balance_change =  col_to_list(df_dict[brand_id])[5]
        wow_free_bet_cnt_change = col_to_list(df_dict[brand_id])[6]
        wow_cash_balance_change = col_to_list(df_dict[brand_id])[7]
        wow_frequency_change = col_to_list(df_dict[brand_id])[8]
        last3wks_ma_frequency = col_to_list(df_dict[brand_id])[9]
        last2wks_ma_frequency = col_to_list(df_dict[brand_id])[10]
        longevity = col_to_list(df_dict[brand_id])[11]
        recency = col_to_list(df_dict[brand_id])[12]
        frequency = col_to_list(df_dict[brand_id])[13]
        churn_factor = col_to_list(df_dict[brand_id])[14]
        league_preference = col_to_list(df_dict[brand_id])[15]
        last_visited_league = col_to_list(df_dict[brand_id])[16]
        sport_visited_start_time = col_to_list(df_dict[brand_id])[17]
        event_type = col_to_list(df_dict[brand_id])[18]


    return dict((zip(list_names, [customers, brand_ids, casino_ids, balances,
                  free_bet_cash_balance, wow_free_balance_change, wow_free_bet_cnt_change,
                  wow_cash_balance_change, wow_frequency_change, 
                  last3wks_ma_frequency, last2wks_ma_frequency,
                  longevity, recency, frequency, churn_factor, league_preference,\
                  last_visited_league, sport_visited_start_time, event_type])))

return_needed_lists_by_brand('2000')
# writing message customers as excel files to keep track of 
now = datetime.now()
reduced_df_send_message_now_lapse_prediction.to_excel(now.strftime('hourly_upload_%Y-%m-%d_%H-%M-%S-%f') + ".xlsx")

def upload_brand(brand_id):   
    
    batch_list = []
    
    if len(df_dict.keys()) == 0:
        return
    if  brand_id not in df_dict.keys():
        return
    for i in range(len(return_needed_lists_by_brand(brand_id)['customer_id'])):
        
        batch = Batch()
        batch.environment = 'production'

        batch.user_identities = {'customer_id': return_needed_lists_by_brand(brand_id)['customer_id'][i]}
        # batch.user_attributes = {
        # 'Casino Id': return_needed_lists_by_brand(brand_id)['casino_id'][i],
        # # 'Balance': return_needed_lists_by_brand(brand_id)['balances'][i],
        # # 'Free Cash Balance': return_needed_lists_by_brand(brand_id)['free_bet_cash_balance'][i],
        # # 'WoW Free Cash Balance Change': return_needed_lists_by_brand(brand_id)['wow_free_balance_change'][i],
        # # 'WoW Free Bet Count Change': return_needed_lists_by_brand(brand_id)['wow_free_bet_cnt_change'][i],
        # # 'WoW Cash Balance Change': return_needed_lists_by_brand(brand_id)['wow_cash_balance_change'][i],
        # # 'WoW Bet Frequency Change': return_needed_lists_by_brand(brand_id)['wow_frequency_change'][i],
        # # 'Last 3 Weeks Moving Average Bet Freq.': return_needed_lists_by_brand(brand_id)['last3wks_ma_frequency'][i],
        # # 'Last 2 Weeks Moving Average Bet Freq.': return_needed_lists_by_brand(brand_id)['last2wks_ma_frequency'][i],
        # 'longevity': return_needed_lists_by_brand(brand_id)['longevity'][i],
        # 'recency': return_needed_lists_by_brand(brand_id)['recency'][i],
        # 'frequency': return_needed_lists_by_brand(brand_id)['frequency'][i],
        # 'Customer Churn Factor Score': return_needed_lists_by_brand(brand_id)['churn_factor'][i],
        # 'League Preference': return_needed_lists_by_brand(brand_id)['league_preference'][i],
        # 'event_type': return_needed_lists_by_brand(brand_id)['event_type'][i]

        #  }
        app_event = mparticle.AppEvent('Customer Lapse Prediciton', 'other',\
                                       custom_attributes = {
                                           #'Casino Id':return_needed_lists_by_brand(brand_id)['casino_id'][i],
                                           #'longevity': return_needed_lists_by_brand(brand_id)['longevity'][i],
                                            'recency': return_needed_lists_by_brand(brand_id)['recency'][i],
                                            'frequency': return_needed_lists_by_brand(brand_id)['frequency'][i],
                                            'Customer Churn Factor Score': return_needed_lists_by_brand(brand_id)['churn_factor'][i],
                                            'League Preference': return_needed_lists_by_brand(brand_id)['league_preference'][i],
                                            'Last Visited League': return_needed_lists_by_brand(brand_id)['last_visited_league'][i],
                                            'Last Visited Sport Session Start': return_needed_lists_by_brand(brand_id)['sport_visited_start_time'][i]
                                           #'event_type': return_needed_lists_by_brand(brand_id)['event_type'][i]
                                           }
                                       )
            
        batch.events = [app_event]
        configuration = mparticle.Configuration()
        configuration.api_key = "us1-172f51d6c6f36b48b892d3deb25d21d8"
        configuration.api_secret = "JVwqPeeJR23G-W6adVsXaT_nI71i6ernjIlBMKszEPCBRGl49DdXCgXmwlTOWd6C"
        api_instance = mparticle.EventsApi(configuration)
        print(batch)

    # mParticle server-to-server endpoint limits one Customer ID per batch
        batch_list.append(batch)
    # try: 
        api_instance.bulk_upload_events(batch_list)
    # except mparticle.ApiResponseErrors as e:
        # print('Exception while calling mParticle: %s\n' % e)

## final upload for customer trigger
for i in df_dict.keys():
    upload_brand(str(i))


## now also including lapsed customer as second trigger event

def return_needed_lists_by_brand(brand_id):
    
    list_names = ['customer_id', 'brand_id', 'casino_id', 'balances',\
                  'free_bet_cash_balance', 'wow_free_balance_change',\
                      'wow_free_bet_cnt_change', 'wow_cash_balance_change',\
                          'wow_frequency_change', 'last3wks_ma_frequency',\
                              'last2wks_ma_frequency', 'longevity', 'recency',\
                                  'frequency', 'churn_factor', 'league_preference',\
                                      'last_visited_league', 'sport_visited_start_time', 'event_type'
                                  
                  ]
        
    if len(df_lapsed_dict.keys()) == 0:
        return  
    if brand_id not in df_lapsed_dict.keys():
        return  
    else:
        customers = col_to_list(df_lapsed_dict[brand_id])[0]
        brand_ids = col_to_list(df_lapsed_dict[brand_id])[1]
        casino_ids = col_to_list(df_lapsed_dict[brand_id])[2]
        balances = col_to_list(df_lapsed_dict[brand_id])[3]
        free_bet_cash_balance = col_to_list(df_lapsed_dict[brand_id])[4]
        wow_free_balance_change =  col_to_list(df_lapsed_dict[brand_id])[5]
        wow_free_bet_cnt_change = col_to_list(df_lapsed_dict[brand_id])[6]
        wow_cash_balance_change = col_to_list(df_lapsed_dict[brand_id])[7]
        wow_frequency_change = col_to_list(df_lapsed_dict[brand_id])[8]
        last3wks_ma_frequency = col_to_list(df_lapsed_dict[brand_id])[9]
        last2wks_ma_frequency = col_to_list(df_lapsed_dict[brand_id])[10]
        longevity = col_to_list(df_lapsed_dict[brand_id])[11]
        recency = col_to_list(df_lapsed_dict[brand_id])[12]
        frequency = col_to_list(df_lapsed_dict[brand_id])[13]
        churn_factor = col_to_list(df_lapsed_dict[brand_id])[14]
        league_preference = col_to_list(df_lapsed_dict[brand_id])[15]
        last_visited_league = col_to_list(df_lapsed_dict[brand_id])[16]
        sport_visited_start_time = col_to_list(df_lapsed_dict[brand_id])[17]
        event_type = col_to_list(df_lapsed_dict[brand_id])[18]


    return dict((zip(list_names, [customers, brand_ids, casino_ids, balances,
                  free_bet_cash_balance, wow_free_balance_change, wow_free_bet_cnt_change,
                  wow_cash_balance_change, wow_frequency_change, 
                  last3wks_ma_frequency, last2wks_ma_frequency,
                  longevity, recency, frequency, churn_factor, league_preference,\
                  last_visited_league, sport_visited_start_time, event_type])))



# writing message customers as excel files to keep track of 
now = datetime.now()
reduced_df_send_message_now_lapsed_customer.to_excel(now.strftime('hourly_upload_%Y-%m-%d_%H-%M-%S-%f') + ".xlsx")

def upload_brand(brand_id):   
    
    batch_list = []
    
    if len(df_lapsed_dict.keys()) == 0:
        return
    if  brand_id not in df_lapsed_dict.keys():
        return
    for i in range(len(return_needed_lists_by_brand(brand_id)['customer_id'])):
        
        batch = Batch()
        batch.environment = 'production'

        batch.user_identities = {'customer_id': return_needed_lists_by_brand(brand_id)['customer_id'][i]}
        # batch.user_attributes = {
        # 'Casino Id': return_needed_lists_by_brand(brand_id)['casino_id'][i],
        # # 'Balance': return_needed_lists_by_brand(brand_id)['balances'][i],
        # # 'Free Cash Balance': return_needed_lists_by_brand(brand_id)['free_bet_cash_balance'][i],
        # # 'WoW Free Cash Balance Change': return_needed_lists_by_brand(brand_id)['wow_free_balance_change'][i],
        # # 'WoW Free Bet Count Change': return_needed_lists_by_brand(brand_id)['wow_free_bet_cnt_change'][i],
        # # 'WoW Cash Balance Change': return_needed_lists_by_brand(brand_id)['wow_cash_balance_change'][i],
        # # 'WoW Bet Frequency Change': return_needed_lists_by_brand(brand_id)['wow_frequency_change'][i],
        # # 'Last 3 Weeks Moving Average Bet Freq.': return_needed_lists_by_brand(brand_id)['last3wks_ma_frequency'][i],
        # # 'Last 2 Weeks Moving Average Bet Freq.': return_needed_lists_by_brand(brand_id)['last2wks_ma_frequency'][i],
        # 'longevity': return_needed_lists_by_brand(brand_id)['longevity'][i],
        # 'recency': return_needed_lists_by_brand(brand_id)['recency'][i],
        # 'frequency': return_needed_lists_by_brand(brand_id)['frequency'][i],
        # 'Customer Churn Factor Score': return_needed_lists_by_brand(brand_id)['churn_factor'][i],
        # 'League Preference': return_needed_lists_by_brand(brand_id)['league_preference'][i],
        # 'event_type': return_needed_lists_by_brand(brand_id)['event_type'][i]

        app_event = mparticle.AppEvent('Lapsed Customers', 'other',\
                                       custom_attributes = {
                                           #'Casino Id':return_needed_lists_by_brand(brand_id)['casino_id'][i],
                                           #'longevity': return_needed_lists_by_brand(brand_id)['longevity'][i],
                                           'recency': return_needed_lists_by_brand(brand_id)['recency'][i],
                                           'frequency': return_needed_lists_by_brand(brand_id)['frequency'][i],
                                           'Customer Churn Factor Score': return_needed_lists_by_brand(brand_id)['churn_factor'][i],
                                           'League Preference': return_needed_lists_by_brand(brand_id)['league_preference'][i],
                                           'Last Visited League': return_needed_lists_by_brand(brand_id)['last_visited_league'][i],
                                           'Last Visited Sport Session Start': return_needed_lists_by_brand(brand_id)['sport_visited_start_time'][i]
                                           #'event_type': return_needed_lists_by_brand(brand_id)['event_type'][i]
                                           }
                                       )       
        batch.events = [app_event]
        configuration = mparticle.Configuration()
        configuration.api_key = "us1-172f51d6c6f36b48b892d3deb25d21d8"
        configuration.api_secret = "JVwqPeeJR23G-W6adVsXaT_nI71i6ernjIlBMKszEPCBRGl49DdXCgXmwlTOWd6C"
        api_instance = mparticle.EventsApi(configuration)
        print(batch)

    # mParticle server-to-server endpoint limits one Customer ID per batch
        batch_list.append(batch)
    # try: 
        api_instance.bulk_upload_events(batch_list)
    # except mparticle.ApiResponseErrors as e:
        # print('Exception while calling mParticle: %s\n' % e)
     
## final upload for customer trigger
for i in df_lapsed_dict.keys():
    upload_brand(str(i))





    